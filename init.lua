vim.notify("Hello!")

-- Settings
require("user.settings")

-- Keybindings
require("user.keybindings")

-- Autocommands
require("user.auto")

-- Plugins
require("user.plugins")

-- LSP
require("user.lsp")

