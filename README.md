# Konstanty's `nvim` Configuration

This repository contains my personal Neovim setup.

## Installation

First, we need to put contents of this repository inside the `~/.config/nvim` directory.
This can be changed by running `nvim -u init.lua` from some other location,
but putting it in `.config` should enable us to use `nvim` directly.

I usually also `alias v='nvim'`, but that's just muscle memory.

Second, we need to get `paq` package manager ready for the first run.
Many people use `packer`, but `paq` is much simpler, so if you don't
need any fancy plugin dependencies or delayed loading, why bother.

```bash
curl -Os "https://raw.githubusercontent.com/savq/paq-nvim/v1.0.3/lua/paq.lua"
```

Then we can

```bash
nvim --headless -c "PaqInstall"
```

If you don't have Rust installed, you can either remove `parinfer` from plugins, or get it installed.
This plugin is very small (and useful if you're writing Scheme), but has no binary releases available.
If it doesn't built automatically, run `cargo build --release` in the fetched plugin repository.

## Folder structure

Configuration is grouped in the `user` directory and is then imported from
`init.lua` with e.g. `require("user.plugins")`.

## Bindings

|Hold|Press|Action|
|----|-----|------|
|Ctrl|  S  | Save |
|Ctrl|  D  | Exit |
|Ctrl|  T  | Terminal |
|Ctrl|  F  | Files |
|Ctrl|  Up  | Move current line up |
|Ctrl|  Down  | Move current line down |
|    | gcc  | Comment toggle line |
|    | gc  | Comment toggle selection |
|    | Enter | Increment TS node selection |
|    | Backspace | Decrement TS node selection |
|    | s[ab] | Show jump options for occurances of "ab", to land on "a" |


