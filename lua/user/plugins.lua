require("paq") {
    "savq/paq-nvim";
    "lewis6991/impatient.nvim";
    "tpope/vim-commentary";
    "tpope/vim-fugitive";
    "tpope/vim-surround";
    "tpope/vim-repeat";
    "neovim/nvim-lspconfig";
    "nvim-treesitter/nvim-treesitter";
    "nvim-lua/plenary.nvim";
    "nvim-telescope/telescope.nvim";
    "ggandor/leap.nvim";
    "akinsho/toggleterm.nvim";
    "lewis6991/gitsigns.nvim";
    "hrsh7th/nvim-cmp";
    "hrsh7th/cmp-buffer";
    "hrsh7th/cmp-path";
    "hrsh7th/cmp-nvim-lsp";
    "jose-elias-alvarez/null-ls.nvim";
    "folke/trouble.nvim";
    "p00f/nvim-ts-rainbow";
    "yorickpeterse/nvim-window";
    {"eraserhd/parinfer-rust", run="cargo build --release"};
}

-- LSP from non-LSP sources
local null_ls = require("null-ls")
null_ls.setup {
    sources = {
        null_ls.builtins.formatting.black,
        null_ls.builtins.formatting.ruff,
        null_ls.builtins.diagnostics.ruff,
    }
}

-- Telescope
local telescope = require('telescope.builtin')
vim.keymap.set('n', 'ff', telescope.find_files, {})
vim.keymap.set('n', 'fg', telescope.live_grep, {})
vim.keymap.set('n', 'fs', telescope.grep_string, {})

-- Git signs
require('gitsigns').setup {
    signcolumn = false,
    numhl = true,
}

-- Improved terminal
require("toggleterm").setup({
  open_mapping = "<c-t>",
  direction = "horizontal",
  shade_terminals = true
})

-- Trouble
require("trouble").setup {
    mode = "document_diagnostics",
    icons = false,
    fold_open = "v",
    fold_closed = ">",
    indent_lines = false,
    padding = false,
    group = false,
    use_diagnostic_signs = false,
    signs = {
        error = "E",
        warning = "W",
        hint = "H",
        information = "I",
        other = "?",
    },
}
vim.keymap.set("n", "<s-t>", "<cmd>TroubleToggle<cr>")

-- Leap
require("leap").add_default_mappings()
require("leap").setup {
    safe_labels = {}
}
vim.keymap.set("n", "<c-_>", function ()
  local current_window = vim.fn.win_getid()
  require('leap').leap { target_windows = { current_window } }
end)

-- Window
require('nvim-window').setup {
    border = 'none'
}

-- Treesitter
require("nvim-treesitter.configs").setup {
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "<cr>",
            node_incremental = "<cr>",
            node_decremental = "<bs>"
        },
    },
    highlight = {
        enable = true
    },
    indent = {
        enable = true
    },
    rainbow = {
        enable = true,
        -- disable = {"lua", "python", "markdown", "bash", "c"},
        extended_mode = false,
        max_file_lines = 10000,
    },
    ensure_installed = {
        "c",
        "lua",
        "python",
        "markdown",
        "bash",
        "json",
        "scheme",
        "html",
    },
}

